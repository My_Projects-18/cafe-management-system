import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit{
products:any;
constructor(private service:EmpService){}
ngOnInit(): void {
  this.service.getAllProducts().subscribe((data:any)=>{
    this.products=data;
    // console.log(data); 
    console.log(this.products); //
    
    });
  }
}
