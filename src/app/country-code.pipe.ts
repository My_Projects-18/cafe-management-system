import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countryCode'
})
export class CountryCodePipe implements PipeTransform {

  transform(countryName:any): any {
    const countryCodes: { [key: string]: string } = {
      'united kingdom': 'UK',
      'australia':'AUS',
      'south africa':'SA',
      'new zealand':'NZD',
      'india':'IND'
    };
    return countryCodes[countryName];

    
   
  }

}
