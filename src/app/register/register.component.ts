import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userData = { userId: '', email: '', password: '', customerName: '', gender: '', country: '', phoneNumber: '', city: '' };
  countries: any;

  constructor(private service: EmpService, private http: HttpClient,private router:Router) { }

  ngOnInit(): void {
    this.service.getAllCountries().subscribe((data: any) => {
      this.countries = data;
    });
  }

  registerUser(): void {
    this.service.registerUser(this.userData).subscribe(
      (response: any) => {
        console.log('Registration successful:', response);
        alert('successfully registerd')
        this.router.navigate(['login']) 
        // Handle success (e.g., display a success message)
      },
      (error: any) => {
        console.error('Registration error:', error);
        // Handle error (e.g., display an error message)
      }
    );
  }
}
