import { Injectable } from "@angular/core";
import { EmpService } from "./emp.service";
import {  Router } from "@angular/router";

@Injectable({   // <-- This is important
  providedIn: 'root'
})
export class AuthGuard {

  constructor(private service:EmpService,private router:Router) {}

  canActivate(): boolean {
    // Check if the user is logged in and return true
if(this.service.getUserLoggedStatus()){
return true;
}else {
  this.router.navigate(['login'])
return false;
}
    // else navigate to login component and return false

  }

}