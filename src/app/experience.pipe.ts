import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'experience'
})
export class ExperiencePipe implements PipeTransform {

  transform(doj: any): any {

    const joinDate = new Date(doj);
    const currentYear = new Date().getFullYear();
  
    const experience = currentYear - joinDate.getFullYear();
  
    return experience + "  years of experience";
  }

}
