import { Component } from '@angular/core';
import { CartItemsService } from '../cart-items.service';
// import { Router } from '@angular/router';

@Component({
  selector: 'app-hot-coffee',
  templateUrl: './hot-coffee.component.html',
  styleUrl: './hot-coffee.component.css'
})
export class HotCoffeeComponent {
  coffees = [
    { 
      name: 'Caramel Macchiato', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100385.jpg', 
      description: 'Freshly steamed milk with vanilla-flavored syrup is marked w...' ,
      price:'MRP = '+350
    },
    { 
      name: 'Chocolate Cappuccino', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/104068.jpg', 
      description: 'Dark, Rich in flavour espresso and bittersweet cocoa lies in...',
      price:'MRP = '+400
    },
    { 
      name: 'Caffe Latte', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100377.jpg', 
      
      description: 'Our dark, Rich in flavour espresso balanced with steamed mil...' ,
      price:'MRP = '+250
    },
    { 
      name: 'Hazelnut Cortado', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/107933.jpg', 
      description: 'A perfect espresso shot and hazelnut syrup, topped with stea...' ,
      price:'MRP = '+400
    },
    { 
      name: 'Caffe Americano', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100433.jpg', 
      description: 'Rich in flavour, full-bodied espresso with hot water in true...',
      price:'MRP = '+600
    },
    { 
      name: 'Solo Espresso', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100511.jpg', 
      description: 'Our smooth signature Espresso Roast and its caramelly sweetn...' ,
      price:'MRP = '+550
    }
    // Add more coffee items as needed
  ];

  // Inject CartService
  constructor(private cartService: CartItemsService) {}

  // Method to add items to the cart
  addToCart(coffee: any): void {
    // Call the addToCart method of the CartService
    this.cartService.addToCart(coffee);
    alert('item added to cart succesfully')
  }
}
