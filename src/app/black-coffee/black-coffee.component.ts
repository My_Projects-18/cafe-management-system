import { Component } from '@angular/core';
import { CartItemsService } from '../cart-items.service';

@Component({
  selector: 'app-black-coffee',
  templateUrl: './black-coffee.component.html',
  styleUrl: './black-coffee.component.css'
})
export class BlackCoffeeComponent {
  coffees = [
    { 
      name: 'Doppie Espresso', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100512.jpg', 
      description: 'Our smooth signature Espresso Roast and its caramelly sweetn...' ,
      price:'MRP = '+350
    },
    { 
      name: 'Black Coffee Americano', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/102149.jpg', 
      description: 'Espresso caffe are topped with water to produce a light laye...',
      price:'MRP = '+400
    },
    { 
      name: 'Pour Over Indian Estate Blends', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100511.jpg', 
      description: 'Pour-over is a beautifully simple and accessible way to brew...' ,
      price:'MRP = '+250
    },
    { 
      name: 'French Press Sumatra-Blend', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100727.jpg', 
      description: 'Brewing with a French press retains the precious natural oil...' ,
      price:'MRP = '+400
    },
    { 
      name: 'French Press Kenya', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100728.jpg', 
      description: 'Brewing with a French press retains the precious natural oil....',
      price:'MRP = '+600
    },
    { 
      name: 'French Press Italian', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100729.jpg', 
      description: 'Brewing with a French press retains the precious natural oil...' ,
      price:'MRP = '+550
    }
    // Add more coffee items as needed
  ];

  // Inject CartService
  constructor(private cartService: CartItemsService) {}

  // Method to add items to the cart
  addToCart(coffee: any): void {
    // Call the addToCart method of the CartService
    this.cartService.addToCart(coffee);
    alert('item added to cart succesfully')
  }
}


