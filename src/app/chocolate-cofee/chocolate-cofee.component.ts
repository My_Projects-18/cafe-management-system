import { Component } from '@angular/core';
import { CartItemsService } from '../cart-items.service';

@Component({
  selector: 'app-chocolate-cofee',
  templateUrl: './chocolate-cofee.component.html',
  styleUrl: './chocolate-cofee.component.css'
})
export class ChocolateCofeeComponent {
  coffees = [
    { 
      name: 'Iced Caffee Americano', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100441.jpg', 
      description: 'Espresso shots are topped with water to produce a light laye...' ,
      price:'MRP = '+350
    },
    { 
      name: 'Iced Chocolate Cappuccino', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/104072.jpg', 
      description: 'Signature Italian style Cappuccino with espresso shot, mocha...' ,
      price:'MRP = '+300
    },
    { 
      name: 'Iced Caffe Mocha', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100399.jpg', 
      description: 'Espresso combined with bittersweet mocha sauce and milk and ...' ,
      price:'MRP = '+450
    },
    { 
      name: 'Iced Cappuccino', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/100427.jpg', 
      description: 'Signature Italian style Cappuccino with espresso shot, steam...' ,
      price:'MRP = '+550
    },
    { 
      name: 'Iced Velvet Vanilla Latte', 
      image: 'https://starbucks-cdn-01.s3.ap-south-1.amazonaws.com/Items/Small/108056.jpg', 
      description: 'Rich in texture, smooth, and creamy latte elevated with vani...' ,
      price:'MRP = '+300
    },
    { 
      name: 'Iced White Mocha', 
      image: 'https://th.bing.com/th/id/OIP.zAmgOrUVa-8YzE4FAy0IJQHaHa?w=204&h=204&c=7&r=0&o=5&dpr=1.3&pid=1.7', 
      description: 'Espresso combined with white mocha sauce and milk over ice. ...' ,
      price:'MRP = '+600
    }
    // Add more coffee items as needed
  ];

  // Inject CartService
  constructor(private cartService: CartItemsService) {}

  // Method to add items to the cart
  addToCart(coffee: any): void {
    // Call the addToCart method of the CartService
    this.cartService.addToCart(coffee);
    alert('item added to cart succesfully')
  }
}
