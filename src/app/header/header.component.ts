import { ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {
  isLoggedIn: boolean = false;

  logout() {
    this.isLoggedIn = false; 
  }

  login() {
    this.isLoggedIn = true; 
  }


}
  // Method to simulate a logout operation
 