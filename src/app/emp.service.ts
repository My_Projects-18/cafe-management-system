import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
isUserLoggedIn:boolean;
  constructor(private http:HttpClient) { 
    this.isUserLoggedIn=false;
  }
  // login
  setUserLoggedIn(){
    this.isUserLoggedIn=true;
  }
  getUserLoggedStatus(){
    return this.isUserLoggedIn
  }
  // logout
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
  }

     getAllCountries(){
      return this.http.get('https://restcountries.com/v3.1/all')
    }
    getAllCustomers(){
      return this.http.get('http://localhost:8085/getAllCustomers')
    }
    getAllProducts(){
      return this.http.get("http://localhost:8085/AllProds")
    }
    registerUser(formData: any): any {
      const url = 'http://localhost:8085/register';
      return this.http.post(url, formData);
    }
    deleteCustomer(customerId: any): any {
      return this.http.delete("http://localhost:8085/deleteCustomerbyid/" + customerId)
    }
    updatecustomer(customers:any){
     
      const url=("http://localhost:8085/updateCustomer/"+customers.customerId);
      return this.http.put(url,customers);
    }
    post(url: string, data: any) {
      return this.http.post(url, data);
    }

    
}
