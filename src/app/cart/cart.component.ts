import { Component } from '@angular/core';
import { CartItemsService } from '../cart-items.service';
import { RouteReuseStrategy, Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {
  cartItems: any[] = [];

  constructor(private cartService: CartItemsService) {
    // Initialize cart items
    this.cartItems = this.cartService.getCartItems();
    
  }
  buy(){
    alert('Order Successful')
  }
  
}
