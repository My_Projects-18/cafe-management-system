import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {
//Dependency Injection for EmpService and Router
constructor(private router: Router, private service: EmpService) {

  
  this.service.setIsUserLoggedOut();  // Set user as logged out

  alert('Successfully Logged Out');
  this.router.navigate(['login']); // Navigate to login page
}
}
