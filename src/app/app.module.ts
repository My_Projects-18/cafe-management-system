import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { ShowEmployeesComponent } from './show-employees/show-employees.component';
import { GenderPipe } from './gender.pipe';
import { ExperiencePipe } from './experience.pipe';
import { CountryCodePipe } from './country-code.pipe';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ShowEmpByIdComponent } from './show-emp-by-id/show-emp-by-id.component';
import { LogoutComponent } from './logout/logout.component';
import { HomeComponent } from './home/home.component';
import { ChocolateCofeeComponent } from './chocolate-cofee/chocolate-cofee.component';
import { HotCoffeeComponent } from './hot-coffee/hot-coffee.component';
import { BlackCoffeeComponent } from './black-coffee/black-coffee.component';
import { CartComponent } from './cart/cart.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { BuyNowComponent } from './buy-now/buy-now.component';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShowEmployeesComponent,
    GenderPipe,
    ExperiencePipe,
    CountryCodePipe,
    ProductComponent,
    RegisterComponent,
    HeaderComponent,
    ShowEmpByIdComponent,
    LogoutComponent,
    HomeComponent,
    ChocolateCofeeComponent,
    HotCoffeeComponent,
    BlackCoffeeComponent,
    CartComponent,
    AboutUsComponent,
    ContactUsComponent,
    BuyNowComponent,
    MessageDialogComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    MatDialogModule
  ],
  providers: [
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
