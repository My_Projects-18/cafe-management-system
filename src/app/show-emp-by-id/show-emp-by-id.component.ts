import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-show-emp-by-id',
  templateUrl: './show-emp-by-id.component.html',
  styleUrl: './show-emp-by-id.component.css'
})
export class ShowEmpByIdComponent implements OnInit {
  emp: any
  customers:any
  constructor(private http:EmpService) {
    
  }
  ngOnInit(): void {
    this.http.getAllCustomers().subscribe( (data : any) => {
      this.customers = data;
    } );
  }
  getEmpById(form:any) :any{
    for(let e of this.customers){
      if(form.empId===e.id){
        this.emp = e;
      }
    }
  }
}
