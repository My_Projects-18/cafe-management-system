import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowEmployeesComponent } from './show-employees/show-employees.component';
import { LoginComponent } from './login/login.component';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';
import { ShowEmpByIdComponent } from './show-emp-by-id/show-emp-by-id.component';

import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { ChocolateCofeeComponent } from './chocolate-cofee/chocolate-cofee.component';
import { HotCoffeeComponent } from './hot-coffee/hot-coffee.component';
import { BlackCoffeeComponent } from './black-coffee/black-coffee.component';
import { CartComponent } from './cart/cart.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { BuyNowComponent } from './buy-now/buy-now.component';

const routes: Routes = [
  {path:'home',component:HomeComponent,canActivate:[AuthGuard]},
  {path:'login', component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'show-employees',component:ShowEmployeesComponent,canActivate:[AuthGuard]},
  {path:'show-products',component:ProductComponent ,canActivate:[AuthGuard]},
  {path:'show-EmpId',component:ShowEmpByIdComponent ,canActivate:[AuthGuard]},
  {path:'logout',component:LogoutComponent},
  {path:'caffee',component:ChocolateCofeeComponent},
  {path:'hot-coffee',component:HotCoffeeComponent},
  {path:'black-coffee',component:BlackCoffeeComponent},
  {path:'cart',component:CartComponent},
  {path:'aboutus',component:AboutUsComponent},
  {path:'contactus',component:ContactUsComponent},
  {path:'buynow',component:BuyNowComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
